#!/usr/bin/env python
# coding: utf-8

import re
import math
from typing import List, Dict

import hostfully
from hostfully import client, error
from hostfully.resource import HostfullyResource
from hostfully.resource.types import reservation_response

from .types import (
    ListingResponse,
    CalendarResponse,
    ReservationResponse,
)

from hostfully.resource.listing import ListingResource
from hostfully.resource.calendar import CalendarResource
from hostfully.resource.reservation import ReservationResource

import json

class HostfullySdk(HostfullyResource):
    """HostfullySdk."""

    def refresh_from(cls, **kwargs):
        """refresh_from."""
        # cls.address1 = kwargs['address1']
        # cls.agencyEmailAddress = kwargs['agencyEmailAddress']
        # cls.city = kwargs['city']
        # cls.countryCode = kwargs['countryCode']
        # cls.defaultCheckInTime = kwargs['defaultCheckInTime']
        # cls.defaultCheckOutTime = kwargs['defaultCheckOutTime']
        cls.name = kwargs['name']
        # cls.phoneNumber = kwargs['phoneNumber']
        # cls.state = kwargs['state']
        cls.uid = kwargs['uid']
        # cls.website = kwargs['website']
        # cls.zipCode = kwargs['zipCode']

        if hostfully.api_key is None:
            raise error.AuthenticationError(
                'No API key provided. (HINT: set your API key using '
                '"hostfully.api_key = <API-KEY>").'
            )

        pattern = r'[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}'  # noqa: E501
        if not re.match(pattern, cls.uid):  # noqa: E501
            raise error.AuthenticationError(
                'Invalid API secret provided. (HINT: XXXXXXXX-XXXX-'
                'XXXX-XXXX-XXXXXXXXXXXX).'
            )

    @classmethod
    def retrieve_url(cls):
        """retrieve_url."""
        return cls.list_url() + 'agencies'

    def get_listings(cls) -> List[ListingResponse]:
        """Returns the dict as a model

        :return: The ListingResponse of this ListingResponse.  # noqa: E501
        :rtype: ListingResponse
        """
        res = {}
        new_listings = []
        limit = 2
        params = {
            'limit': limit,
            'offset': 0,
        }
        query = '''query {{
            properties(agencyUid: "{uid}") {{
                UID
                address1
                address2
                baseDailyRate
                baseGuests
                bathrooms
                bedCount
                bedrooms
                city
                cleaningFeeAmount
                countryCode
                currency
                currencySymbol
                defaultCheckinTime
                defaultCheckoutTime
                isActive
                latitude
                longitude
                name
                picture
                postalCode
                propertyURL
                state
                # bookingWindow
            }}
        }}'''.format(uid=cls.uid)
        res = client.post(ListingResource.list_url(), {'query': query}, params)
        # print('INIT LISTING: RESP: {}'.format(json.dumps(res, indent=4, sort_keys=True)))
        res = res['properties']
        
        # Pagination
        total_count = 0
        if 'count' in res:
            total_count = res['count']
        if 'limit' in res:
            limit = res['limit']
        num_pages = math.ceil(total_count / limit)

        # First Results
        new_listings.extend([ListingResponse(**l) for l in res])
       
        # More Results
        for page in range(1, num_pages):
            params = {}
            params['skip'] = page * limit
            params['limit'] = limit
            res = client.post(ListingResource.list_url(), {'query': query}, params)
            # print('MORE LISTING: RESP: {}'.format(json.dumps(res, indent=4, sort_keys=True)))
            res = res['properties']
            new_listings.extend([ListingResponse(**i) for i in res])
        return new_listings

    # @cached_property
    def get_calendar(
        cls, 
        id: str,
        start_date: str,
        end_date: str
    ) -> CalendarResponse:
        """Returns the dict as a model

        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')

        params = {
            'from': start_date,
            'to': end_date,
        }
        res = {}
        resp = client.get(CalendarResource.get_url(id), params)
        res['days'] = resp['entries']
        return CalendarResponse(**res)

    # @cached_property
    def get_reservations(
        cls, 
        id: str, 
        start_date: str, 
        end_date: str
    ) -> List[ReservationResponse]:
        """Returns the dict as a model

        :return: The ReservationResponse of this ReservationResponse.  # noqa: E501
        :rtype: ReservationResponse
        """

        if not isinstance(start_date, str):
            raise error.InvalidRequestError('Start Date')

        if not isinstance(end_date, str):
            raise error.InvalidRequestError('End Date')

        new_reservations = []
        limit = 100
        offset = 0
        res = {}

        params = {
            'agencyUid': cls.uid,
            'offset': offset,
            'limit': limit,
            'check_in_from': start_date,
            'check_in_to': end_date,
        }
        res['result'] = []
        query = '''query {{
            leads(propertyUid: "{uid}") {{
                leadType
                uid
                propertyUid
                status
                checkInDateTimeString
                checkOutDateTimeString
                bookedDateTimeString
                externalBookingID
                order {{
                  uid
                  currency
                  totalGrossOrderValue
                }}
            }}
        }}'''.format(uid=id)
        res = client.post(ReservationResource.list_url(), {'query': query}, params)
        # print('INIT RES: RESP: {}'.format(json.dumps(res, indent=4, sort_keys=True)))
        res = res['leads']
        marker = len(res) == limit
        new_reservations.extend(ReservationResponse(**r) for r in res)
        while marker:
            offset += limit
            res = {}
            res['result'] = []
            params['offset'] = offset
            params['limit'] = limit
            
            if client.get_env() == 'sandbox':
                res = []
            else:
                res = client.post(ReservationResource.list_url(), {'query': query}, params)
                res = res['leads']
            
            # print('MORE RES: RESP: {}'.format(json.dumps(res, indent=4, sort_keys=True)))
            marker = len(res) == limit
            new_reservations.extend([ReservationResponse(**r) for r in res])

        return [res for res in new_reservations if res.order]

    def update_calendar(
        cls,
        id: str,
        batch_array: List[Dict]
    ) -> Dict:
        """Returns the dict as a model

        :return: The dict of this dict.  # noqa: E501
        :rtype: dict
        """
        def get_arrays_from_array(batch_days_array: List[Dict]) -> List[List[Dict]]:
            updated_calendar_arrays = []
            updated_calendar_days = []
            sbb_batch_max = 200
            for day in batch_days_array:
                updated_calendar_days.append(day)
                if len(updated_calendar_days) == sbb_batch_max:
                    updated_calendar_arrays.append(updated_calendar_days)
                    updated_calendar_days = []

            # Add non 200 batch (last)
            if len(updated_calendar_days) > 0:
                updated_calendar_arrays.append(updated_calendar_days)
            return updated_calendar_arrays
        
        arrays = get_arrays_from_array(batch_array)
        results = []
        for array in arrays:
            payload = {
                'pricingperiods': array
            }
            res = client.post(CalendarResource.post_url(), payload, None)
            results.append(res)
        return results

    def __unicode__(cls):
        """__unicode__."""
        return '<{} {}>'.format(cls.__class__.__name__, cls.id)
