#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyAccountResource


class ListingResource(HostfullyAccountResource):

    @classmethod
    def list_url(cls) -> str:
        """
        Gets the GET url of this ListingResource

        :return: The GET url of this ListingResource.
        :rtype: str
        """
        return super(ListingResource, cls).list_url() + 'propertiesql'