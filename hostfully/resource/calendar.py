#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyAccountResource


class CalendarResource(HostfullyAccountResource):

    @classmethod
    def get_url(cls, id: str) -> str:
        """
        Gets the GET url of this CalendarResource

        :return: The GET url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'propertycalendar/' + id

    @classmethod
    def post_url(cls) -> str:
        """
        Gets the POST url of this CalendarResource

        :return: The POST url of this CalendarResource.
        :rtype: str
        """
        return super(CalendarResource, cls).list_url() + 'pricingperiodsbulk'