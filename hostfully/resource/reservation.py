#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyAccountResource


class ReservationResource(HostfullyAccountResource):

    @classmethod
    def list_url(cls) -> str:
        """
        Gets the GET url of this ReservationResource

        :return: The GET url of this ReservationResource.
        :rtype: str
        """
        return super(ReservationResource, cls).list_url() + 'leadsql'