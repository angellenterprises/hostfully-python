#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyResource
from typing import List, Dict


class Availability(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'unavailable': True,
    }

    model_types = {
        'unavailable': bool,
    }

    attribute_map = {
        'unavailable': 'unavailable',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Availability of this Availability.  # noqa: E501
        :rtype: Availability
        """
        cls.sanity_check(kwargs)

        cls._unavailable = None

        cls.unavailable = kwargs['unavailable']

    @property
    def unavailable(self) -> bool:
        """Gets the unavailable of this Availability.


        :return: The unavailable of this Availability.
        :rtype: bool
        """
        return self._unavailable

    @unavailable.setter
    def unavailable(self, unavailable: bool):
        """Sets the unavailable of this Availability.


        :param unavailable: The unavailable of this Availability.
        :type unavailable: bool
        """
        if unavailable is None:
            raise ValueError("Invalid value for `unavailable`, must not be `None`")  # noqa: E501

        self._unavailable = unavailable


class Pricing(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'value': True,
    }

    model_types = {
        'value': float,
    }

    attribute_map = {
        'value': 'value',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Pricing of this Pricing.  # noqa: E501
        :rtype: Pricing
        """
        cls.sanity_check(kwargs)

        cls._value = None

        cls.value = kwargs['value']

    @property
    def value(self) -> float:
        """Gets the value of this Pricing.


        :return: The value of this Pricing.
        :rtype: float
        """
        return self._value

    @value.setter
    def value(self, value: float):
        """Sets the value of this Pricing.


        :param value: The value of this Pricing.
        :type value: float
        """
        if value is None:
            raise ValueError("Invalid value for `value`, must not be `None`")  # noqa: E501

        self._value = value


class Day(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        '_date': True,
        'availability': True,
        'pricing': True,
    }

    model_types = {
        '_date': str,
        'availability': dict,
        'pricing': dict
    }

    attribute_map = {
        '_date': 'date',
        'availability': 'availability',
        'pricing': 'pricing'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Day of this Day.  # noqa: E501
        :rtype: Day
        """
        cls.sanity_check(kwargs)

        cls.__date = None
        cls._availability = None
        cls._pricing = None

        cls._date = kwargs['date']
        cls.availability = Availability(**kwargs['availability'])
        cls.pricing = Pricing(**kwargs['pricing'])

    @property
    def _date(self) -> str:
        """Gets the _date of this Day.


        :return: The _date of this Day.
        :rtype: str
        """
        return self.__date

    @_date.setter
    def _date(self, _date: str):
        """Sets the _date of this Day.


        :param _date: The _date of this Day.
        :type _date: str
        """
        if _date is None:
            raise ValueError("Invalid value for `_date`, must not be `None`")  # noqa: E501

        self.__date = _date

    @property
    def availability(self) -> Availability:
        """Gets the availability of this Day.


        :return: The availability of this Day.
        :rtype: Availability
        """
        return self._availability

    @availability.setter
    def availability(self, availability: Availability):
        """Sets the availability of this Day.


        :param availability: The availability of this Day.
        :type availability: Availability
        """
        if availability is None:
            raise ValueError("Invalid value for `availability`, must not be `None`")  # noqa: E501

        self._availability = availability

    @property
    def pricing(self) -> Pricing:
        """Gets the pricing of this Day.


        :return: The pricing of this Day.
        :rtype: Pricing
        """
        return self._pricing

    @pricing.setter
    def pricing(self, pricing: Pricing):
        """Sets the pricing of this Day.


        :param pricing: The pricing of this Day.
        :type pricing: Pricing
        """
        if pricing is None:
            raise ValueError("Invalid value for `pricing`, must not be `None`")  # noqa: E501

        self._pricing = pricing

class CalendarResponse(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'days': True,
    }

    model_types = {
        'days': list,
    }

    attribute_map = {
        'days': 'days',
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The CalendarResponse of this CalendarResponse.  # noqa: E501
        :rtype: CalendarResponse
        """
        cls.sanity_check(kwargs)
        cls._days = None

        cls.days = [Day(**d) for d in kwargs['days']]

    @property
    def days(self) -> List[Day]:
        """Gets the days of this CalendarResponse.


        :return: The days of this CalendarResponse.
        :rtype: List[Day]
        """
        return self._days

    @days.setter
    def days(self, days: List[Day]):
        """Sets the days of this CalendarResponse.


        :param days: The days of this CalendarResponse.
        :type days: List[Day]
        """
        if days is None:
            raise ValueError("Invalid value for `days`, must not be `None`")  # noqa: E501

        self._days = days