#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyResource
from typing import List, Dict


class ListingResponse(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'uid': True,
        'is_active': True,
        'base_guests': True,
        'city': True,
        'address1': True,
        'state': True,
        'postal_code': True,
        'country_code': True,
        'latitude': True,
        'longitude': True,
        'bathrooms': True,
        'bedrooms': True,
        'bed_count': True,
        'default_checkin_time': True,
        'default_checkout_time': True,
        'name': True,
        'picture': True,
        'cleaning_fee_amount': True,
        'base_daily_rate': True,
        'currency': True
    }

    model_types = {
        'uid': str,
        'is_active': bool,
        'base_guests': int,
        'city': str,
        'address1': str,
        'state': str,
        'postal_code': str,
        'country_code': str,
        'latitude': float,
        'longitude': float,
        'bathrooms': str,
        'bedrooms': int,
        'bed_count': int,
        'default_checkin_time': int,
        'default_checkout_time': int,
        'name': str,
        'picture': str,
        'cleaning_fee_amount': float,
        'base_daily_rate': float,
        'currency': str
    }

    attribute_map = {
        'uid': 'UID',
        'is_active': 'isActive',
        'base_guests': 'baseGuests',
        'city': 'city',
        'address1': 'address1',
        'state': 'state',
        'postal_code': 'postalCode',
        'country_code': 'countryCode',
        'latitude': 'latitude',
        'longitude': 'longitude',
        'bathrooms': 'bathrooms',
        'bedrooms': 'bedrooms',
        'bed_count': 'bedCount',
        'default_checkin_time': 'defaultCheckinTime',
        'default_checkout_time': 'defaultCheckoutTime',
        'name': 'name',
        'picture': 'picture',
        'cleaning_fee_amount': 'cleaningFeeAmount',
        'base_daily_rate': 'baseDailyRate',
        'currency': 'currency'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)
        cls._uid = None
        cls._is_active = None
        cls._base_guests = None
        cls._city = None
        cls._address1 = None
        cls._state = None
        cls._postal_code = None
        cls._country_code = None
        cls._latitude = None
        cls._longitude = None
        cls._bathrooms = None
        cls._bedrooms = None
        cls._bed_count = None
        cls._default_checkin_time = None
        cls._default_checkout_time = None
        cls._name = None
        cls._picture = None
        cls._cleaning_fee_amount = None
        cls._base_daily_rate = None
        cls._currency = None

        cls.uid = kwargs['UID']
        cls.is_active = kwargs['isActive']
        cls.base_guests = kwargs['baseGuests']
        cls.city = kwargs['city']
        cls.address1 = kwargs['address1']
        cls.state = kwargs['state']
        cls.postal_code = kwargs['postalCode']
        cls.country_code = kwargs['countryCode']
        cls.latitude = kwargs['latitude']
        cls.longitude = kwargs['longitude']
        cls.bathrooms = kwargs['bathrooms']
        cls.bedrooms = kwargs['bedrooms']
        cls.bed_count = kwargs['bedCount']
        cls.default_checkin_time = kwargs['defaultCheckinTime']
        cls.default_checkout_time = kwargs['defaultCheckoutTime']
        cls.name = kwargs['name']
        cls.picture = kwargs['picture']
        cls.cleaning_fee_amount = kwargs['cleaningFeeAmount']
        cls.base_daily_rate = kwargs['baseDailyRate']
        cls.currency = kwargs['currency']

    @property
    def uid(self) -> str:
        """Gets the uid of this ListingResponse.


        :return: The uid of this ListingResponse.
        :rtype: str
        """
        return self._uid

    @uid.setter
    def uid(self, uid: str):
        """Sets the uid of this ListingResponse.


        :param uid: The uid of this ListingResponse.
        :type uid: str
        """
        if uid is None:
            raise ValueError("Invalid value for `uid`, must not be `None`")  # noqa: E501

        self._uid = uid

    @property
    def is_active(self) -> bool:
        """Gets the is_active of this ListingResponse.


        :return: The is_active of this ListingResponse.
        :rtype: bool
        """
        return self._is_active

    @is_active.setter
    def is_active(self, is_active: bool):
        """Sets the is_active of this ListingResponse.


        :param is_active: The is_active of this ListingResponse.
        :type is_active: bool
        """
        if is_active is None:
            raise ValueError("Invalid value for `is_active`, must not be `None`")  # noqa: E501

        self._is_active = is_active

    @property
    def base_guests(self) -> int:
        """Gets the base_guests of this ListingResponse.


        :return: The base_guests of this ListingResponse.
        :rtype: int
        """
        return self._base_guests

    @base_guests.setter
    def base_guests(self, base_guests: int):
        """Sets the base_guests of this ListingResponse.


        :param base_guests: The base_guests of this ListingResponse.
        :type base_guests: int
        """
        if base_guests is None:
            raise ValueError("Invalid value for `base_guests`, must not be `None`")  # noqa: E501

        self._base_guests = base_guests

    @property
    def city(self) -> str:
        """Gets the city of this ListingResponse.


        :return: The city of this ListingResponse.
        :rtype: str
        """
        return self._city

    @city.setter
    def city(self, city: str):
        """Sets the city of this ListingResponse.


        :param city: The city of this ListingResponse.
        :type city: str
        """
        if city is None:
            raise ValueError("Invalid value for `city`, must not be `None`")  # noqa: E501

        self._city = city

    @property
    def address1(self) -> str:
        """Gets the address1 of this ListingResponse.


        :return: The address1 of this ListingResponse.
        :rtype: str
        """
        return self._address1

    @address1.setter
    def address1(self, address1: str):
        """Sets the address1 of this ListingResponse.


        :param address1: The address1 of this ListingResponse.
        :type address1: str
        """
        if address1 is None:
            raise ValueError("Invalid value for `address1`, must not be `None`")  # noqa: E501

        self._address1 = address1

    @property
    def state(self) -> str:
        """Gets the state of this ListingResponse.


        :return: The state of this ListingResponse.
        :rtype: str
        """
        return self._state

    @state.setter
    def state(self, state: str):
        """Sets the state of this ListingResponse.


        :param state: The state of this ListingResponse.
        :type state: str
        """
        if state is None:
            raise ValueError("Invalid value for `state`, must not be `None`")  # noqa: E501

        self._state = state

    @property
    def postal_code(self) -> str:
        """Gets the postal_code of this ListingResponse.


        :return: The postal_code of this ListingResponse.
        :rtype: str
        """
        return self._postal_code

    @postal_code.setter
    def postal_code(self, postal_code: str):
        """Sets the postal_code of this ListingResponse.


        :param postal_code: The postal_code of this ListingResponse.
        :type postal_code: str
        """
        if postal_code is None:
            raise ValueError("Invalid value for `postal_code`, must not be `None`")  # noqa: E501

        self._postal_code = postal_code

    @property
    def country_code(self) -> str:
        """Gets the country_code of this ListingResponse.


        :return: The country_code of this ListingResponse.
        :rtype: str
        """
        return self._country_code

    @country_code.setter
    def country_code(self, country_code: str):
        """Sets the country_code of this ListingResponse.


        :param country_code: The country_code of this ListingResponse.
        :type country_code: str
        """
        if country_code is None:
            raise ValueError("Invalid value for `country_code`, must not be `None`")  # noqa: E501

        self._country_code = country_code

    @property
    def latitude(self) -> float:
        """Gets the latitude of this ListingResponse.


        :return: The latitude of this ListingResponse.
        :rtype: float
        """
        return self._latitude

    @latitude.setter
    def latitude(self, latitude: float):
        """Sets the latitude of this ListingResponse.


        :param latitude: The latitude of this ListingResponse.
        :type latitude: float
        """
        if latitude is None:
            raise ValueError("Invalid value for `latitude`, must not be `None`")  # noqa: E501

        self._latitude = latitude

    @property
    def longitude(self) -> float:
        """Gets the longitude of this ListingResponse.


        :return: The longitude of this ListingResponse.
        :rtype: float
        """
        return self._longitude

    @longitude.setter
    def longitude(self, longitude: float):
        """Sets the longitude of this ListingResponse.


        :param longitude: The longitude of this ListingResponse.
        :type longitude: float
        """
        if longitude is None:
            raise ValueError("Invalid value for `longitude`, must not be `None`")  # noqa: E501

        self._longitude = longitude

    @property
    def bathrooms(self) -> str:
        """Gets the bathrooms of this ListingResponse.


        :return: The bathrooms of this ListingResponse.
        :rtype: str
        """
        return self._bathrooms

    @bathrooms.setter
    def bathrooms(self, bathrooms: str):
        """Sets the bathrooms of this ListingResponse.


        :param bathrooms: The bathrooms of this ListingResponse.
        :type bathrooms: str
        """
        if bathrooms is None:
            raise ValueError("Invalid value for `bathrooms`, must not be `None`")  # noqa: E501

        self._bathrooms = bathrooms

    @property
    def bedrooms(self) -> int:
        """Gets the bedrooms of this ListingResponse.


        :return: The bedrooms of this ListingResponse.
        :rtype: int
        """
        return self._bedrooms

    @bedrooms.setter
    def bedrooms(self, bedrooms: int):
        """Sets the bedrooms of this ListingResponse.


        :param bedrooms: The bedrooms of this ListingResponse.
        :type bedrooms: int
        """
        if bedrooms is None:
            raise ValueError("Invalid value for `bedrooms`, must not be `None`")  # noqa: E501

        self._bedrooms = bedrooms

    @property
    def bed_count(self) -> int:
        """Gets the bed_count of this ListingResponse.


        :return: The bed_count of this ListingResponse.
        :rtype: int
        """
        return self._bed_count

    @bed_count.setter
    def bed_count(self, bed_count: int):
        """Sets the bed_count of this ListingResponse.


        :param bed_count: The bed_count of this ListingResponse.
        :type bed_count: int
        """
        if bed_count is None:
            raise ValueError("Invalid value for `bed_count`, must not be `None`")  # noqa: E501

        self._bed_count = bed_count

    @property
    def default_checkin_time(self) -> int:
        """Gets the default_checkin_time of this ListingResponse.


        :return: The default_checkin_time of this ListingResponse.
        :rtype: int
        """
        return self._default_checkin_time

    @default_checkin_time.setter
    def default_checkin_time(self, default_checkin_time: int):
        """Sets the default_checkin_time of this ListingResponse.


        :param default_checkin_time: The default_checkin_time of this ListingResponse.
        :type default_checkin_time: int
        """
        if default_checkin_time is None:
            raise ValueError("Invalid value for `default_checkin_time`, must not be `None`")  # noqa: E501

        self._default_checkin_time = default_checkin_time

    @property
    def default_checkout_time(self) -> int:
        """Gets the default_checkout_time of this ListingResponse.


        :return: The default_checkout_time of this ListingResponse.
        :rtype: int
        """
        return self._default_checkout_time

    @default_checkout_time.setter
    def default_checkout_time(self, default_checkout_time: int):
        """Sets the default_checkout_time of this ListingResponse.


        :param default_checkout_time: The default_checkout_time of this ListingResponse.
        :type default_checkout_time: int
        """
        if default_checkout_time is None:
            raise ValueError("Invalid value for `default_checkout_time`, must not be `None`")  # noqa: E501

        self._default_checkout_time = default_checkout_time

    @property
    def name(self) -> str:
        """Gets the name of this ListingResponse.


        :return: The name of this ListingResponse.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this ListingResponse.


        :param name: The name of this ListingResponse.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def picture(self) -> str:
        """Gets the picture of this ListingResponse.


        :return: The picture of this ListingResponse.
        :rtype: str
        """
        return self._picture

    @picture.setter
    def picture(self, picture: str):
        """Sets the picture of this ListingResponse.


        :param picture: The picture of this ListingResponse.
        :type picture: str
        """
        if picture is None:
            raise ValueError("Invalid value for `picture`, must not be `None`")  # noqa: E501

        self._picture = picture

    @property
    def cleaning_fee_amount(self) -> float:
        """Gets the cleaning_fee_amount of this ListingResponse.


        :return: The cleaning_fee_amount of this ListingResponse.
        :rtype: float
        """
        return self._cleaning_fee_amount

    @cleaning_fee_amount.setter
    def cleaning_fee_amount(self, cleaning_fee_amount: float):
        """Sets the cleaning_fee_amount of this ListingResponse.


        :param cleaning_fee_amount: The cleaning_fee_amount of this ListingResponse.
        :type cleaning_fee_amount: float
        """
        # if cleaning_fee_amount is None:
            # raise ValueError("Invalid value for `cleaning_fee_amount`, must not be `None`")  # noqa: E501

        self._cleaning_fee_amount = cleaning_fee_amount

    @property
    def base_daily_rate(self) -> float:
        """Gets the base_daily_rate of this ListingResponse.


        :return: The base_daily_rate of this ListingResponse.
        :rtype: float
        """
        return self._base_daily_rate

    @base_daily_rate.setter
    def base_daily_rate(self, base_daily_rate: float):
        """Sets the base_daily_rate of this ListingResponse.


        :param base_daily_rate: The base_daily_rate of this ListingResponse.
        :type base_daily_rate: float
        """
        if base_daily_rate is None:
            raise ValueError("Invalid value for `base_daily_rate`, must not be `None`")  # noqa: E501

        self._base_daily_rate = base_daily_rate

    @property
    def currency(self) -> str:
        """Gets the currency of this ListingResponse.


        :return: The currency of this ListingResponse.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency: str):
        """Sets the currency of this ListingResponse.


        :param currency: The currency of this ListingResponse.
        :type currency: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        self._currency = currency