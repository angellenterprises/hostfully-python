#!/usr/bin/env python
# coding: utf-8

from hostfully.resource import HostfullyResource
from typing import List, Dict

class Order(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    required = {
        'uid': True,
        'currency': True,
        'total_gross_order_value': True,
    }

    model_types = {
        'uid': str,
        'currency': str,
        'total_gross_order_value': float
    }

    attribute_map = {
        'uid': 'uid',
        'currency': 'currency',
        'total_gross_order_value': 'totalGrossOrderValue'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)

        cls._uid = None
        cls._currency = None
        cls._total_gross_order_value = None

        cls.uid = kwargs['uid']
        cls.currency = kwargs['currency']
        cls.total_gross_order_value = kwargs['totalGrossOrderValue']

    @property
    def uid(cls) -> str:
        """Gets the uid of this Order.


        :return: The uid of this Order.
        :rtype: str
        """
        return cls._uid

    @uid.setter
    def uid(cls, uid: str):
        """Sets the uid of this Order.


        :param uid: The uid of this Order.
        :type uid: str
        """
        if uid is None:
            raise ValueError("Invalid value for `uid`, must not be `None`")  # noqa: E501

        cls._uid = uid

    @property
    def currency(cls) -> str:
        """Gets the currency of this Order.


        :return: The currency of this Order.
        :rtype: str
        """
        return cls._currency

    @currency.setter
    def currency(cls, currency: str):
        """Sets the currency of this Order.


        :param currency: The currency of this Order.
        :type currency: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")  # noqa: E501

        cls._currency = currency

    @property
    def total_gross_order_value(cls) -> float:
        """Gets the total_gross_order_value of this Order.


        :return: The total_gross_order_value of this Order.
        :rtype: float
        """
        return cls._total_gross_order_value

    @total_gross_order_value.setter
    def total_gross_order_value(cls, total_gross_order_value: float):
        """Sets the total_gross_order_value of this Order.


        :param total_gross_order_value: The total_gross_order_value of this Order.
        :type total_gross_order_value: float
        """
        if total_gross_order_value is None:
            raise ValueError("Invalid value for `total_gross_order_value`, must not be `None`")  # noqa: E501

        cls._total_gross_order_value = total_gross_order_value


class ReservationResponse(HostfullyResource):
    """
    Attributes:
      model_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    nullable = {
        'external_booking_id': True,
        'booked_date_time_string': True,
    }

    required = {
        'uid': True,
        'external_booking_id': True,
        'property_uid': True,
        'check_in_date_time_string': True,
        'check_out_date_time_string': True,
        'booked_date_time_string': True,
        'status': True,
        'order': False
    }

    model_types = {
        'uid': str,
        'external_booking_id': str,
        'property_uid': str,
        'check_in_date_time_string': str,
        'check_out_date_time_string': str,
        'booked_date_time_string': str,
        'status': str,
        'order': dict
    }

    attribute_map = {
        'uid': 'uid',
        'external_booking_id': 'externalBookingID',
        'property_uid': 'propertyUid',
        'check_in_date_time_string': 'checkInDateTimeString',
        'check_out_date_time_string': 'checkOutDateTimeString',
        'booked_date_time_string': 'bookedDateTimeString',
        'status': 'status',
        'order': 'order'
    }

    def refresh_from(cls, **kwargs):
        """Returns the dict as a model

        :param kwargs: A dict.
        :type: dict
        :return: The Currency of this Currency.  # noqa: E501
        :rtype: Currency
        """
        cls.sanity_check(kwargs)

        cls._uid = None
        cls._external_booking_id = None
        cls._property_uid = None
        cls._check_in_date_time_string = None
        cls._check_out_date_time_string = None
        cls._booked_date_time_string = None
        cls._status = None
        cls._order = None

        cls.uid = kwargs['uid']
        cls.external_booking_id = kwargs['externalBookingID']
        cls.property_uid = kwargs['propertyUid']
        cls.check_in_date_time_string = kwargs['checkInDateTimeString']
        cls.check_out_date_time_string = kwargs['checkOutDateTimeString']
        cls.booked_date_time_string = kwargs['bookedDateTimeString']
        cls.status = kwargs['status']
        if 'order' in kwargs and kwargs['order']:
            cls.order = Order(**kwargs['order'])

    @property
    def uid(cls) -> str:
        """Gets the uid of this ReservationResponse.


        :return: The uid of this ReservationResponse.
        :rtype: str
        """
        return cls._uid

    @uid.setter
    def uid(cls, uid: str):
        """Sets the uid of this ReservationResponse.


        :param uid: The uid of this ReservationResponse.
        :type uid: str
        """
        if uid is None:
            raise ValueError("Invalid value for `uid`, must not be `None`")  # noqa: E501

        cls._uid = uid

    @property
    def external_booking_id(cls) -> str:
        """Gets the external_booking_id of this ReservationResponse.


        :return: The external_booking_id of this ReservationResponse.
        :rtype: str
        """
        return cls._external_booking_id

    @external_booking_id.setter
    def external_booking_id(cls, external_booking_id: str):
        """Sets the external_booking_id of this ReservationResponse.


        :param external_booking_id: The external_booking_id of this ReservationResponse.
        :type external_booking_id: str
        """
        # if external_booking_id is None:
        #     raise ValueError("Invalid value for `external_booking_id`, must not be `None`")  # noqa: E501

        cls._external_booking_id = external_booking_id

    @property
    def property_uid(cls) -> str:
        """Gets the property_uid of this ReservationResponse.


        :return: The property_uid of this ReservationResponse.
        :rtype: str
        """
        return cls._property_uid

    @property_uid.setter
    def property_uid(cls, property_uid: str):
        """Sets the property_uid of this ReservationResponse.


        :param property_uid: The property_uid of this ReservationResponse.
        :type property_uid: str
        """
        if property_uid is None:
            raise ValueError("Invalid value for `property_uid`, must not be `None`")  # noqa: E501

        cls._property_uid = property_uid

    @property
    def check_in_date_time_string(cls) -> str:
        """Gets the check_in_date_time_string of this ReservationResponse.


        :return: The check_in_date_time_string of this ReservationResponse.
        :rtype: str
        """
        return cls._check_in_date_time_string

    @check_in_date_time_string.setter
    def check_in_date_time_string(cls, check_in_date_time_string: str):
        """Sets the check_in_date_time_string of this ReservationResponse.


        :param check_in_date_time_string: The check_in_date_time_string of this ReservationResponse.
        :type check_in_date_time_string: str
        """
        if check_in_date_time_string is None:
            raise ValueError("Invalid value for `check_in_date_time_string`, must not be `None`")  # noqa: E501

        cls._check_in_date_time_string = check_in_date_time_string

    @property
    def check_out_date_time_string(cls) -> str:
        """Gets the check_out_date_time_string of this ReservationResponse.


        :return: The check_out_date_time_string of this ReservationResponse.
        :rtype: str
        """
        return cls._check_out_date_time_string

    @check_out_date_time_string.setter
    def check_out_date_time_string(cls, check_out_date_time_string: str):
        """Sets the check_out_date_time_string of this ReservationResponse.


        :param check_out_date_time_string: The check_out_date_time_string of this ReservationResponse.
        :type check_out_date_time_string: str
        """
        if check_out_date_time_string is None:
            raise ValueError("Invalid value for `check_out_date_time_string`, must not be `None`")  # noqa: E501

        cls._check_out_date_time_string = check_out_date_time_string

    @property
    def booked_date_time_string(cls) -> str:
        """Gets the booked_date_time_string of this ReservationResponse.


        :return: The booked_date_time_string of this ReservationResponse.
        :rtype: str
        """
        return cls._booked_date_time_string

    @booked_date_time_string.setter
    def booked_date_time_string(cls, booked_date_time_string: str):
        """Sets the booked_date_time_string of this ReservationResponse.


        :param booked_date_time_string: The booked_date_time_string of this ReservationResponse.
        :type booked_date_time_string: str
        """
        # if booked_date_time_string is None:
        #     raise ValueError("Invalid value for `booked_date_time_string`, must not be `None`")  # noqa: E501

        cls._booked_date_time_string = booked_date_time_string

    @property
    def status(cls) -> str:
        """Gets the status of this ReservationResponse.


        :return: The status of this ReservationResponse.
        :rtype: str
        """
        return cls._status

    @status.setter
    def status(cls, status: str):
        """Sets the status of this ReservationResponse.


        :param status: The status of this ReservationResponse.
        :type status: str
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        cls._status = status

    @property
    def order(cls) -> Order:
        """Gets the order of this ReservationResponse.


        :return: The order of this ReservationResponse.
        :rtype: Order
        """
        return cls._order

    @order.setter
    def order(cls, order: Order):
        """Sets the order of this ReservationResponse.


        :param order: The order of this ReservationResponse.
        :type order: Order
        """
        # if order is None:
        #     raise ValueError("Invalid value for `order`, must not be `None`")  # noqa: E501

        cls._order = order