class hostfullyError(Exception):
    def __init__(self, error, status_code=None, headers=None):
        super(hostfullyError, self).__init__(error)

        self.error = error
        self.status_code = status_code
        self.headers = headers

    def __unicode__(self):
        return self.error


class APIError(hostfullyError):
    pass


class APIConnectionError(hostfullyError):
    pass


class InvalidRequestError(hostfullyError):
    pass


class AuthenticationError(hostfullyError):
    pass


class NotIntegrated(hostfullyError):
    pass
