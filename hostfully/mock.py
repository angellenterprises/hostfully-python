"""mock.py."""
import os
import random
import uuid
from datetime import datetime, timedelta
from hostfully.util import read_json, write_json


def get_month_start(year=None, month=None):
    """get_month_start."""
    return datetime.strptime('{}-{}-{}'.format(year, month, 1), '%Y-%m-%d')


def get_month_end(dt=None):
    """get_month_end."""
    next_dt = dt.replace(day=28) + timedelta(days=4)  # this will never fail
    return next_dt - timedelta(days=next_dt.day)


def backwards_month(month=0):
    """backwards_month."""
    if month == 0:
        return 12
    if month == -1:
        return 11
    if month == -2:
        return 10
    if month == -3:
        return 9
    if month == -4:
        return 8
    if month == -5:
        return 7
    if month == -6:
        return 6
    if month == -7:
        return 5
    if month == -8:
        return 4
    if month == -9:
        return 3
    if month == -10:
        return 2
    if month == -11:
        return 1


def dates_bwn_twodates(start_date, end_date):
    """dates_bwn_twodates."""
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(n)


def to_dt(string):
    """to_dt."""
    return datetime.strptime(string, "%Y-%m-%d")


def to_dtl(string):
    """to_dtl."""
    return datetime.strptime(string, "%Y-%m-%d %H:%M:%S")


def from_dt(date):
    """from_dt."""
    return datetime.strftime(date, "%Y-%m-%d")


def from_dtl(date):
    """from_dtl."""
    return datetime.strftime(date, "%Y-%m-%d %H:%M:%S")


def random_blocks(selected):
    """random_blocks."""
    return 'manual'


def random_price():
    """random_price."""
    return random.randint(200, 250)


def parse_status(selected):
    """random_price."""
    if selected == 'new':
        return 'unavailable'
    if selected == 'modified':
        return 'unavailable'
    if selected == 'ownerStay':
        return 'unavailable'
    return 'available'


def init_mock_calendar(id=None, params=None):
    """random_price."""
    start_at = datetime.now()
    end_at = datetime.now()
    if not params:
        raise ValueError('Invalid Start / End Date')

    start_at = to_dt(params['startDate'])
    end_at = to_dt(params['endDate'])

    day_list = []
    for date in dates_bwn_twodates(start_at, end_at):
        types = ['new', 'modified', 'cancelled', 'ownerStay']
        selected = types[random.randint(0, len(types) - 1)]
        day = {
            'id': '1',
            'date': from_dt(date),
            'listingId': '1',
            'price': random_price(),
            'isAvailable': True,
            'status': parse_status(selected),
        }
        day_list.append(day)
    return day_list


def get_mock_calendar(id=None, params=None):
    """get_mock_calendar."""
    path = os.path.abspath(
        'services/hostfully/mocks/calendar/{}.json'.format(id)
    )
    json_data = read_json(path)
    if not json_data:
        initial_calendar = init_mock_calendar(id=id, params=params)
        write_json(initial_calendar, path)
        return initial_calendar
    return json_data


def get_random_channel():
    """get_random_channel."""
    platforms = ['airbnb', 'homeaway', 'other']
    return platforms[random.randint(0, len(platforms)-1)]


def get_random_status():
    """get_random_status."""
    statuss = ['confirmed', 'unconfirmed']
    return statuss[random.randint(0, len(statuss)-1)]


def get_random_checkin():
    """get_random_checkin."""
    check_in = datetime.now()
    return check_in + timedelta(days=random.randint(1, 21))


def get_random_fare(nights):
    """get_random_fare."""
    return random.randint(99, 400) * nights


def get_day_spread(res_days=None):
    """get_day_spread."""
    full_res_days = []
    for res in res_days:
        if 'arrivalDate' not in res or 'departureDate' not in res:
            raise ValueError('Invalid Datetime Object in Day Spread Days')

        rounded_start = res['arrivalDate'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        rounded_end = res['departureDate'].replace(
            hour=0,
            minute=1,
            second=0,
            microsecond=0
        )
        number_days = (rounded_end - rounded_start).days
        for i in range(number_days + 1):
            new_start = rounded_start + timedelta(days=i)
            full_res_days.append(datetime.strftime(new_start, "%Y-%m-%d"))
            i += 1
    return full_res_days


def init_mock_reservations(
    no_list=[],
    id=None,
    start_date=None,
    end_date=None
):
    """init_mock_reservations."""
    res_list = []
    num_reservations = random.randint(0, 3)
    for check_in in range(num_reservations):
        nights = random.randint(2, 7)
        check_in = get_random_checkin()
        check_out = check_in + timedelta(days=nights)

        # NO DEPLICATES
        res_spread = get_day_spread([{
            'arrivalDate': check_in,
            'departureDate': check_out - timedelta(days=1),
        }])
        if check_in in no_list:
            num_reservations + 1
            continue

        for res_day in res_spread:
            if res_day in no_list:
                num_reservations + 1
                continue

        id = str(uuid.uuid4())
        code = str(uuid.uuid4())
        guest = str(uuid.uuid4())
        reservation = {
            'id': random.randint(0, 999999),
            'listingMapId': random.randint(0, 999999),
            'channelId': random.randint(0, 999999),
            'channelName': get_random_channel(),
            'reservationId': id,
            'hostfullyReservationId': random.randint(0, 999999),
            'channelReservationId': guest,
            "arrivalDate": from_dt(check_in),
            "departureDate": from_dt(check_out),
            'reservationDate': from_dtl(datetime.now()),
            'nights': nights,
            'confirmationCode': ''.join(
                x.strip() for x in code.split('-')
            )[:6],
            'status': 'new',
            'currency': 'USD',
            'totalPrice': get_random_fare(nights),
            'cleaningFee': 125,
        }
        res_list.append(reservation)
    return res_list


def get_mock_reservations(id=None, start_date=None, end_date=None):
    """get_mock_reservations."""
    path = os.path.abspath(
        'services/hostfully/mocks/reservations/{}.json'.format(id)
    )
    json_data = read_json(path)
    if json_data == []:
        initial_reservations = init_mock_reservations(
            id=id,
            start_date=start_date,
            end_date=end_date
        )
        write_json(initial_reservations, path)
        return initial_reservations

    res_days = []
    for reservation in json_data:
        res_dict = {
            'arrivalDate': to_dt(reservation['arrivalDate']),
            'departureDate': to_dt(reservation['departureDate']) - timedelta(days=1),
        }
        res_days.append(res_dict)

    no_list = get_day_spread(res_days)
    if len(no_list) > 20:
        return json_data

    new_reservations = json_data + init_mock_reservations(
        id=id,
        no_list=no_list,
        start_date=start_date,
        end_date=end_date
    )
    write_json(new_reservations, path)
    return new_reservations


def get_mock_agencies():
    """get_mock_agencies."""
    return [
        {
            "address1": "450 Brentwood Drive",
            "agencyEmailAddress": "michele@hostfully.com",
            "city": "Austin, TX",
            "countryCode": "VN",
            "defaultCheckInTime": 15,
            "defaultCheckOutTime": 11,
            "name": "Your Agency",
            "phoneNumber": "512-348-5683",
            "state": "Texas",
            "uid": "4c3d6e8c-1912-4755-8fd8-ac5cc61abe58",
            "website": "http://www.itography.com",
            "zipCode": "78737"
        },
        {
            "address1": "20 Main Street",
            "agencyEmailAddress": "info@crosspollinate.org",
            "city": "San Francisco",
            "countryCode": "US",
            "defaultCheckInTime": 15,
            "defaultCheckOutTime": 11,
            "name": "Vacation Rentals",
            "phoneNumber": "800-585-5584",
            "state": "CA",
            "uid": "f9f01864-6b28-4b3e-9340-7dabfc4d0f82",
            "website": "http://vacation-rental.com",
            "zipCode": "94102"
        },
        {
            "address1": None,
            "agencyEmailAddress": "denis@harpangell.com",
            "city": "St. Louis",
            "countryCode": "US",
            "defaultCheckInTime": 15,
            "defaultCheckOutTime": 11,
            "name": "Nightpricer",
            "phoneNumber": None,
            "state": None,
            "uid": "c1f4b168-3431-4378-b44c-c1707c529868",
            "website": "nightpricer.com",
            "zipCode": None
        }
    ]

def get_mock_listings():
    """get_mock_listings."""
    return [
        {
            "acceptBookingRequest": False,
            "acceptInstantBook": False,
            "address1": "1485 Waller st",
            "address2": None,
            "airBnBID": None,
            "areaSize": 3000,
            "areaSizeUnit": "SQUARE_FEET",
            "availabilityCalendarUrl": "https://sandbox.hostfully.com/calendar/29876.ics?itp=1",
            "baseDailyRate": 450.0,
            "baseGuests": 6,
            "bathrooms": "2",
            "bedCount": 0,
            "bedTypes": {
                "airMattress": 0,
                "bunkBed": 0,
                "couch": 0,
                "crib": 0,
                "doubleBed": 0,
                "floorMattress": 0,
                "futonBed": 0,
                "hammockBed": 0,
                "kingBed": 0,
                "queenBed": 0,
                "singleBed": 0,
                "sofaBed": 0,
                "toddlerBed": 0,
                "waterBed": 0
            },
            "bedrooms": 3,
            "bookingLeadTime": 48,
            "bookingWindow": -1,
            "bookingWindowAfterCheckout": -1,
            "cancellationPolicy": None,
            "city": "San Francisco",
            "cleaningFeeAmount": 150.0,
            "countryCode": "US",
            "createdDate": 1634184572000,
            "currency": "USD",
            "currencySymbol": "$",
            "defaultCheckinTime": 15,
            "defaultCheckoutTime": 11,
            "externalID": None,
            "extraGuestFee": 10.0,
            "floor": 3,
            "fullPaymentTiming": 45,
            "guideBookUrl": None,
            "homeAwayID": None,
            "isActive": True,
            "latitude": 37.768728,
            "listingLinks": {
                "airbnbUrl": None,
                "bookingDotComUrl": None,
                "homeAwayUrl": None,
                "hostfullyUrl": "https://sandbox.hostfully.com/vacation-rental-property-v2/29876/victorian-house-%28sample%29",
                "hvmiUrl": None,
                "tripAdvisorUrl": None
            },
            "longitude": -122.448056,
            "maximumGuests": 8,
            "maximumStay": 0,
            "minimumStay": 2,
            "minimumWeekendStay": 0,
            "name": "Victorian House (Sample)",
            "panoramicDataUrl": None,
            "percentUponReservation": 50,
            "picture": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRfTA6qg3wFq2p6hf8jI9a68AR_tmA1mEmAnL3xijG6742a3r1B",
            "postalCode": "94117",
            "pricingRules": None,
            "propertyURL": "https://sandbox.hostfully.com/vacation-rental-property-v2/29876/victorian-house-%28sample%29",
            "rentalCondition": None,
            "rentalLicenseNumber": None,
            "rentalLicenseNumberExpirationDate": None,
            "reviews": {
                "average": None,
                "total": 0
            },
            "securityDepositAmount": 500.0,
            "state": "California",
            "taxationRate": 9.0,
            "turnOverDays": 0,
            "type": "SAMPLE",
            "uid": "075c810a-064a-4c35-b75b-7a01be0aeee4",
            "webLink": None,
            "weekEndRatePercentAdjustment": 0.0,
            "wifiNetwork": None,
            "wifiPassword": None
        }
    ]
