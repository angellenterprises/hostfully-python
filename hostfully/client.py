#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
import logging
import requests
import textwrap
from base64 import b64encode

from hostfully import error


# Create Logger
logger = logging.getLogger('app')

def build_url():
    from hostfully import env, api_base, api_version
    url = api_base + '/' + api_version + '/'
    if env == 'sandbox':
        url = "https://sandbox-api.hostfully.com" + '/' + api_version + '/'

    return url


def get_env():
    from hostfully import env

    if not env:
        raise error.AuthenticationError(
            'No ENV provided. (HINT: set your ENV using '
            '"hostfully.env = <sandbox>"). '
        )

    return env

def get_auth_headers():
    return {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def headers():
    from hostfully import api_key

    return {
        'X-HOSTFULLY-APIKEY': api_key,
        'accept': 'application/json',
        'content-type': 'application/json',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


def get(url, params):
    try:
        print('GET: {}'.format(url))
        res = requests.get(url, params=params, headers=headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def put(url, json_res, params):
    try:
        print('PUT: {}'.format(url))
        res = requests.put(
            url, 
            params=params, 
            json=json_res, 
            headers=headers()
        )
    except Exception as e:
        handle_request_error(e)

    return res.text


def post(url, json_res, params):
    try:
        print('POST: {}'.format(url))
        res = requests.post(
            url,
            params=params,
            json=json_res,
            headers=headers()
        )
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)

def delete(url):
    try:
        res = requests.delete(url, headers=headers())
    except Exception as e:
        handle_request_error(e)

    return handle_response(res)


def handle_response(res):
    try:
        json_res = res.json()
    except ValueError as e:
        handle_parse_error(e)

    if not (200 <= res.status_code < 300):
        handle_error_code(json_res, res.status_code, res.headers)

    return json_res


def handle_request_error(e):
    if isinstance(e, requests.exceptions.RequestException):
        msg = 'Unexpected error communicating with Hostfully.'
        err = '{}: {}'.format(type(e).__name__, str(e))
    else:
        msg = ('Unexpected error communicating with Hostfully. '
               'It looks like there\'s probably a configuration '
               'issue locally.')
        err = 'A {} was raised'.format(type(e).__name__)
        if u'%s' % e:
            err += ' with error message {}'.format(e)
        else:
            err += ' with no error message'

    msg = textwrap.fill(msg) + '\n\n(Network error: {})'.format(err)
    raise error.APIConnectionError(msg)


def handle_error_code(json_res, status_code, headers):
    handle_rate_limit(headers)
    if status_code == 400:
        err = json_res.get('apiErrorMessage', 'Bad request')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 401:
        err = json_res.get('apiErrorMessage', 'Not authorized')
        raise error.AuthenticationError(err, status_code, headers)
    elif status_code == 403:
        err = json_res.get('apiErrorMessage', 'Not authorized')
        raise error.AuthenticationError(err, status_code, headers)
    elif status_code == 404:
        err = json_res.get('apiErrorMessage', 'Not found')
        raise error.InvalidRequestError(err, status_code, headers)
    elif status_code == 500:
        err = json_res.get('apiErrorMessage', 'Internal server error')
        raise error.APIError(err, status_code, headers)
    else:
        err = json_res.get('apiErrorMessage', 'Unknown status code ({})'.format(status_code))
        raise error.APIError(err, status_code, headers)

def handle_rate_limit(headers):
    if not isinstance(headers, dict):
        return
    
    limit = 0
    remaining = 0
    if 'x-ratelimit-limit' in headers:
        print(headers['x-ratelimit-limit'])
        limit = int(headers['x-ratelimit-limit'])
    if 'x-ratelimit-remaining' in headers:
        print(headers['x-ratelimit-remaining'])
        remaining = headers['x-ratelimit-remaining']

    if remaining < 10:
        err = 'Rate limit exceeded ({})'.format(remaining)
        raise error.APIError(err, 999, headers)


def handle_parse_error(e, status_code=None, headers=None):
    err = '{}: {}'.format(type(e).__name__, e)
    msg = 'Error parsing hostfully JSON response. \n\n{}'.format(err)
    raise error.APIError(msg, status_code, headers)
