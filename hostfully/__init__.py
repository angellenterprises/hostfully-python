"""__init__."""
api_key = None
api_version = 'v2'
env = 'production'
api_base = 'https://api.hostfully.com'

from hostfully.resource.base import (  # noqa
    HostfullySdk,
)
