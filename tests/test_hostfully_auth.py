"""test_hostfully_sdk.py."""
from testing_config import BaseTestConfig
from datetime import datetime, timedelta
from unittest.mock import Mock, patch

import hostfully

# import pytest
# @pytest.mark.skip(reason="Using Prod Cert")
class TestHostfullyAuth(BaseTestConfig):
    """TestHostfullyAuth."""

    sdk: hostfully.HostfullySdk = None

    @patch('hostfully.client.requests.get')
    def test_hostfully_auth_invalid_api_key(cls, mock_get):
        print('should fail to construct w/out api key')
        try:
            hostfully.env = 'sandbox'
            hostfully.api_key = None

            mock_get.return_value = Mock(status_code=403)
            mock_get.return_value.json.return_value = cls.json_fixtures['auth']['401']

            sdk = hostfully.HostfullySdk.get(cls.json_fixtures['api']['secret'])
        except Exception as e:
            cls.assertEqual(str(e), 'Could not identify the requesting entity')

    @patch('hostfully.client.requests.get')
    def test_hostfully_auth_invalid_agency_id(cls, mock_get):
        print('should fail to construct w/out api key')
        try:
            hostfully.env = 'sandbox'
            hostfully.api_key = cls.json_fixtures['api']['key']

            mock_get.return_value = Mock(status_code=403)
            mock_get.return_value.json.return_value = cls.json_fixtures['auth']['402']

            sdk = hostfully.HostfullySdk.get(cls.json_fixtures['api']['id'])
        except Exception as e:
            cls.assertEqual(str(e), 'The requested account has not been integrated with Nightpricer Please verify your Agency UID and try again.')