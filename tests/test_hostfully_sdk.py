#!/usr/bin/env python
# coding: utf-8

from testing_config import BaseTestConfig
from unittest.mock import Mock, patch

from datetime import datetime, timedelta

import hostfully

# import pytest
# @pytest.mark.skip(reason="Using Prod Cert")
class TestHostfullySDK(BaseTestConfig):
    """TestHostfullySDK."""

    sdk: hostfully.HostfullySdk = None

    @patch('hostfully.client.requests.get')
    def setUp(cls, mock_get):
        print('should auth')

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['agencies']

        hostfully.api_key = cls.json_fixtures['api']['key']
        cls.sdk = hostfully.HostfullySdk.get(cls.json_fixtures['api']['secret'])

    # TEST: get_listing
    @patch('hostfully.client.requests.post')
    def test_hostfully_get_listings(cls, mock_post):
        print('should get listings')

        mock_post.return_value = Mock(status_code=200)
        mock_post.return_value.json.return_value = cls.json_fixtures['properties']

        response = cls.sdk.get_listings()

        cls.listings = []
        for listing in response:
            cls.listings.append(listing.uid)
        cls.assertEqual(len(response), 1)
        print('TESTED: get_listing')

    # TEST: get_calendar
    @patch('hostfully.client.requests.get')
    def test_hostfully_get_calendar(cls, mock_get):
        print('should get calendar')
        
        start_date = datetime.now()
        end_date = start_date + timedelta(days=365)

        start_date = datetime.strftime(start_date, '%Y-%m-%d')
        end_date = datetime.strftime(end_date, '%Y-%m-%d')

        mock_get.return_value = Mock(status_code=200)
        mock_get.return_value.json.return_value = cls.json_fixtures['calendar']['get']
        
        response = cls.sdk.get_calendar(
            '075c810a-064a-4c35-b75b-7a01be0aeee4',
            start_date,  # MUST BE STRING DATE
            end_date  # MUST BE STRING DATE
        )
        cls.assertEqual(
            len(response.to_dict()['days']), 
            len(cls.json_fixtures['calendar']['get']['entries'])
        )
        print('TESTED: get_calendar')

    # TEST: get_reservations
    @patch('hostfully.client.requests.post')
    def test_hostfully_get_reservations(cls, mock_post):
        print('should get reservations')
        
        now = datetime.now()
        start_date = now
        end_date = now + timedelta(days=10)

        start_date = datetime.strftime(start_date, '%Y-%m-%d')
        end_date = datetime.strftime(end_date, '%Y-%m-%d')

        mock_post.return_value = Mock(status_code=200)
        mock_post.return_value.json.return_value = cls.json_fixtures['reservations']['post']

        l = ['075c810a-064a-4c35-b75b-7a01be0aeee4']
        for uid in l:
            response = cls.sdk.get_reservations(
                uid,
                start_date,  # MUST BE STRING DATE
                end_date  # MUST BE STRING DATE
            )
            print(response)
            cls.assertEqual(len(response), 2)
            print('TESTED: get_reservations')

    # TEST: update_calendar
    @patch('hostfully.client.requests.post')
    def test_hostfully_update_calendar(cls, mock_post):
        print('should update calendar days')
        batch_array = [{
                'propertyUid': '075c810a-064a-4c35-b75b-7a01be0aeee4',
                'isCheckinAllowed': "true",
                'from': '2022-07-29',
                'to': '2022-07-29',
                'amount': 100,
                'minimumStay': 1,
            },
            {
                'propertyUid': '075c810a-064a-4c35-b75b-7a01be0aeee4',
                'isCheckinAllowed': "true",
                'from': '2022-07-30',
                'to': '2022-07-30',
                'amount': 100,
                'minimumStay': 1,
        }]

        mock_post.return_value = Mock(status_code=200)
        mock_post.return_value.json.return_value = cls.json_fixtures['calendar']['post']

        response = cls.sdk.update_calendar(
            'id',
            batch_array,
        )
        cls.assertEqual(response, [cls.json_fixtures['calendar']['post']])
        print('TESTED: update_calendar')
