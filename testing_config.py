from unittest import TestCase


from hostfully.util import read_json


class BaseTestConfig(TestCase):

    json_fixtures = {}

    @classmethod
    def setUpClass(cls):
        print('Set Up Class Testing')
        cls.json_fixtures = read_json('./tests/fixtures/hostfully_api.json')
        
    @classmethod
    def tearDownClass(cls):
        print('Tear Down Class Testing')